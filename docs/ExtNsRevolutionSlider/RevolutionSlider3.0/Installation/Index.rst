.. include:: ../Includes.txt

.. _installation:

============
Installation
============

License Activation & Installation
=================================

To activate license and install this premium TYPO3 product, Please refer this documentation https://docs.t3planet.com/en/latest/License/Index.html

.. attention:: For TYPO3 >= v11 composer-based TYPO3 instance, Please don't forget to run below commands.

.. code-block:: python

   php vendor/bin/typo3 extension:setup
   php vendor/bin/typo3 nsrevolution:setup


Check System Requirements
=========================

   .. rst-class:: bignums

   #. Switch to NITSAN > Revolution Slider

   #. Click on "Updates" and check system requirements. Please make sure to have all green-signals ;) If something is wrong then adjust your server according to needs.

   .. figure:: Images/TYPO3-Revolution-Slider-System-Check.png
       :alt: TYPO3-Revolution-Slider-System-Check
       :width: 1000px