.. include:: ../Includes.txt

==========
FAQ Plugin
==========

To display FAQs at frontend, you need to add FAQs plugin.

.. figure:: Images/add_plugin.jpeg
   :alt: Add FAQ Plugin

Once you add FAQ plugin you can configure how to display FAQs at frontend.

.. figure:: Images/configure_plugin.jpeg
   :alt: Configure Plugin

- **What would you like to display?:-** Here you can either select List or FAQs Submission Form. List option will display FAQs at frontend.

- **Select FAQs Style:-** Here you can decide how to display FAQs. There are 3 options: 
    1) **Category wise List**: You can display all the FAQs of selected categories & sub-categories. FAQs will be grouped by Category.
    2) **FAQ List**: You can display all the FAQs stored in Record Storage page.
    3) **Selected FAQs List**: You can display list of Selected FAQs.

- **Record Storage Page:-** Select Storage pages where FAQs/Categories are stored. 