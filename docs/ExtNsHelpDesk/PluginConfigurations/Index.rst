.. include:: ../Includes.txt

=====================
Plugin Configurations
=====================

You can do the plugin configurations from backend according to your need. There are basically 3 options available which are Listing view, User Registration Form, & Ticket Submission form.
You can choose a user registraion form for front end user registration, listing view helps you to display the all tickets on front end & can create the ticket with the help of the ticket submission form option easily.


**Step 1.** Create New Content Element > Insert Plugin > helpdesk

**Step 2.** Click on "Plugin" menu

**Step 3.** Select option from "Select View" dropdown box. which you want to create on your frontend.

**Step 4.** Configure necessary settings like, insert page id, item to show per page etc.


.. figure:: Images/ns-helpdesk-typo3-Plugin-Configurations.jpeg
   :alt: ns-helpdesk-typo3-Plugin-Configurations