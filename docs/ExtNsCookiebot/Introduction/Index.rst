	
.. include:: ../Includes.txt

============
Introduction
============

EXT:ns_cookiebot
================

.. figure:: Images/ext-cookiebot.jpg
   :alt: Extension Cookibot Banner 

What does it do?
================

EXT:ns_cookiebot facilitates the website holder by displaying a cookie statement bar at every page of the website till the visitor accepts it.

Installing CookieBot on a TYPO3 site is now made very easy. You do not need to change the source code of your template, cookie statements can be quickly and easily placed on your site.


Helpful Links
=============

.. Note::

	- Product: https://t3planet.com/cookiebot-gdpr-compliant-typo3-extension-free
	- TYPO3 Backend Live Demo: https://demo.t3planet.com/live-typo3/t3t-extensions/typo3/?TYPO3_AUTOLOGIN_USER=editor-cookiebot
	- Front End Demo: https://demo.t3planet.com/t3-extensions/cookiebot
	- To make any domain-related changes or whitelist any development and staging domains, please reach our support center.- https://t3planet.com/support
