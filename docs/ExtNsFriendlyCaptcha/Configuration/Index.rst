.. include:: ../Includes.txt

=============
Configuration
=============

You can configure all the settings of ns_friendlycaptcha as described below:

- **Step 1:** Go to Template.

- **Step 2:** Select root page.

- **Step 3:** Select Constant Editor > Plugin.NSFRIENDLYCAPTCHA (13).

- **Step 4:** Now, you can configure all the options which you want eg., Auto Check,Check on focus and Manual, See below screenshot.

.. figure:: Images/Friendlycaptcha_Configuration_1.jpeg
   :alt:  Configuration1
   
.. figure:: Images/Friendlycaptcha_Configuration_2.jpeg
   :alt:  Configuration2