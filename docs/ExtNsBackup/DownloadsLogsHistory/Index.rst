.. include:: ../Includes.txt

========================
Download, Logs & History
========================

You can easily download your already taken backups, also chekcout Logs & History.

**Step 1.** Go to Admin Tools > NS Backup

**Step 2.** Click on "Backup History" menu

**Step 3.** In Actions column, "Download Backup | Logs | Delete" backup.

.. figure:: Images/ns-backup-typo3-history-logs.png
   :alt: ns-backup-typo3-history-logs

.. figure:: Images/ns-backend-typo3-logs.png
   :alt: ns-backend-typo3-logs