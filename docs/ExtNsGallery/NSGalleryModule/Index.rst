.. include:: ../Includes.txt

==========================
Gallery Backend Module
==========================

.. Attention::
   After Activation of gallery extension you must have to define the storage folder page id from constant editor. Refer below image please.!

.. figure:: Images/nsgallery_constanteditor_setting.jpeg
   :alt: NS Gallery Constant Editor Setting
   
Once you perform installation process successfully, you will find a new Backend module "Gallery" under "Nitsan" group in Sidebar. This module will be used for all the configuration of all Galleries in your site. This Module provides complete management of Galleries.

.. figure:: Images/backend_module.jpeg
   :alt: Backend Module

This Module contains various tabs for all kind of Galleries Configurations. Let's understand how each tab works one by one.


Dashboard
=========

This is the first tab in NS Gallery Module and as name suggest, it shows dashboard of complete module. It highlights total number of albums created at the currently selected page. It also displays how many Images/Media are created and used in various albums.

.. figure:: Images/dashboard.jpeg
   :alt: Dashboard

Albums
======

This tab allows administrator to create albums. Albums are collection of Media (Images & Videos). User can add and edit albums from this tab.

.. figure:: Images/albums_listing.jpeg
   :alt: Albums Management

To add a new album, click on "Add new album" button. It will open a Album Record where you can add Media in album. You can set Album Title and add all Images/Videos as individual Media in Album Record.

.. figure:: Images/add_album.jpeg
   :alt: Add a new album

Once Album is created, you just need to select the album in Gallery plugin.

Global Settings
===============

This tab is used to configure Global Settings for the Gallery extension. Here you can set configuration for appearance of Gallery, What to show in Lightbox and also settings for Slider, Carousel & YouTube Videos related settings. 

.. figure:: Images/global_settings.jpeg
   :alt: How to configure Global Settings?

