.. include:: ../Includes.txt

============
Editor Guide
============

After Setup of your Lovely Site follow below steps for making it standalone & versatile.

**Step 1.**	Create a standard Page on your TYPO3 setup.

**Step 2.**	Make it Root page from TYPO3 page property > Behavior Tab.

.. figure:: Images/MakeRootPage.jpeg
   :alt: ns-theme-t3karma-editor-guide-makearootpage

**Step 3.**	Go to TYPO3 Template module & Create a template of your page.

.. figure:: Images/CreateTemplate.jpeg
   :alt: ns-theme-t3karma-editor-guide-CreateTemplate

**Step 4.** Now include all required Extension for that Go to Info/Modify from Template module. Make sure the order of the included extension. First it should be a EXT:Fluid Content Element & atlast EXT:Parent Theme & EXT:Child Theme respectively.

.. figure:: Images/InfoModifyIncludeOrder.jpeg
   :alt: ns-theme-t3karma-editor-guide-IncludeOrder

**Step 5.** Its important to  setup home page & main menu id so for that Go to Theme Options > General Tab - here you have to define root page id on the Home Page id & Main menu ID. Next, Its optional to configure all other settings.!

.. figure:: Images/SetupPageID.jpeg
   :alt: ns-theme-t3karma-editor-guide-SetupPageID

**Step 6.** Site Configuration > Setup youe website title, identifier and entry point.

.. figure:: Images/SiteConfiguration.jpeg
   :alt: ns-theme-t3karma-editor-guide-Site-Configuration

**Step 7.** If you want to your website with multilanguage than Goto Site Configuration > Language Tab > Create New Language. After this, Insert each required details.!

.. figure:: Images/MultiLanguage.jpeg
   :alt: ns-theme-t3karma-editor-guide-MultiLanguage
   
**Step 8.** Now Go to Page Module > TYPO3 Standard/Root Page > Page Property > Appearance Tab. Select the Backend & frontend Page layout accordingly your webpage layout.

.. figure:: Images/PageLayout.jpeg
   :alt: ns-theme-t3karma-editor-guide-PageLayout
  
**Step 9.** Heart feature of the Website is a Style. To apply it on your website - Goto Theme Options > Style Tab. 

Here you can configure the Header/Footer Setting, Color Schema, Navigation styles, Hover effect, Menu style, Font style, Loader Style etc.

Into the General Tab you can Enable & Disable Breadcrumb, Searchbar, Multilanguage Menu, Maintenance Mode, Speed Performance setting & much more.

.. figure:: Images/WebsiteStyle.jpeg
   :alt: ns-theme-t3karma-editor-guide-WebsiteStyle

.. attention:: How To enable Mega menu on my Website ?
Answer is, Enable the option from the General Tab of Page Property. that's it Now your page having the mega menu.

.. figure:: Images/MegaMenu.jpeg
   :alt: ns-theme-t3karma-editor-guide-MegaMenu.jpeg

.. note:: For Subtitles - Add subtitle on the the General Tab of Page Property. Remember this will only visible on frontend if you have selected the naviagtion layout as "subtitle" from theme Option.
 
.. figure:: Images/MenuSubtitle.jpeg
   :alt: ns-theme-t3karma-editor-guide-MenuSubtitle
   
.. figure:: Images/ThemeOptionSubtitle.jpeg
   :alt: ns-theme-t3karma-editor-guide-ThemeOptionSubtitle

**Step 10.** Inserting the Container Element & Custom Element on your webpage. 
You can use the Custom & TYPO3 Default element within the container to provide more flexibility in content area.!

.. figure:: Images/ContainerGrid.jpeg
   :alt: ns-theme-t3karma-editor-guide-ContainerGrid
   
.. figure:: Images/CustomElement.jpeg
   :alt: ns-theme-t3karma-editor-guide-CustomElement

.. Tip:: How to easy start; It’s all about we highly recommend to go with our demo-pages https://demo.t3planet.com/t3-karma/demos/