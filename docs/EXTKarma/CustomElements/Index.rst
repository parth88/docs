.. include:: ../Includes.txt

.. |true-icon| image:: Images/righ-icon.png
.. |false-icon| image:: Images/crose-icon.png
.. |view-icon| image:: Images/view.png
.. |version-icon| image:: Images/version.png
.. |documentation-icon| image:: Images/documentation.png
.. |typo-icon| image:: Images/typo.png


.. _Action_And_Results:

================
Custom Elements
================

Template's Elements
===================

Whenever you are going to add a new element, in wizard you can find "Custom Elements" tab where template related custom elements been configured. All the Content Elements are self explanatory.

.. figure:: Images/custom_elements1.jpeg
   :alt: Custom Elements
   
.. figure:: Images/custom_elements2.jpeg
   :alt: Custom Elements
   
.. figure:: Images/custom_elements3.jpeg
   :alt: Custom Elements