.. include:: ../Includes.txt

.. _installation:

=======================
Migration from v2 to v3
=======================

What's TYPO3 revolution slider v3? T3Planet is now official partner of revolution slider core guys - Themepunch. Team T3Planet feeling proud to launch v3 which includes whole WordPress slider revolution plugin into TYPO3.

We have major breaking changes with v3, now whole approach to create slider is changed. You will able to create slider as just like in WordPress version.

.. attention:: You will need to manually re-create all your sliders in v3. Unfortunately technically it's very difficult to setup migration from v2 to v3.

.. rst-class:: bignums-xxl

**Steps to Migration**

   You will need to completely delete existing EXT.ns_revolution_slider v2 extension from your TYPO3 Instance; and Install new v3. Please follows the Step-by-step Guide to migrate from TYPO3 revolution slider v2 to v3.

   .. rst-class:: bignums

   #. Before start, please make sure to take backup of your TYPO3 website.

   #. Please make sure to latest released version EXT.ns_license https://extensions.typo3.org/extension/ns_license

   #. Go to Admin Tools > Extensions Manager > De-Activate EXT.ns_revolution_slider v2.

   #. Go to Admin Tools > Extensions Manager > Delete EXT.ns_revolution_slider from your Extension manager or composer.

   #. Go to NITSAN > License Management > De-Activate License, Check https://docs.t3planet.com/en/latest/License/LicenseDeActivation/Index.html#how-to-de-activate-license-key

   #. Follow Installation Steps at https://docs.t3planet.com/en/latest/ExtNsRevolutionSlider/RevolutionSlider3.0/Installation/Index.html

   #. Follow Configuration Steps at https://docs.t3planet.com/en/latest/ExtNsRevolutionSlider/RevolutionSlider3.0/Configuration/Index.html