.. include:: ../Includes.txt

=============
Configuration
=============

Configure Default Settings
==========================

You can set default setting like storage folder of maintenance or Admin email address at Constants.

.. figure:: Images/default_settings.jpeg
   :alt: Default Settings 

**Admin Email:** Email set here will get subscription email from visitors if Subscription box is added on Maintenance screen. 

Configure Maintenance Mode Settings
===================================

Once you have installed extension successfully, you will find **Maintenance Mode** module in Web section in sidebar. 

.. figure:: Images/maintenance_module.jpeg
   :alt: Maintenance Module 

Select Root page to apply Maintenance Mode to whole site. Click on Maintenance Mode module. You will find Maintenance settings as below:

.. figure:: Images/maintenance_settings.jpeg
   :alt: Maintenance Settings

- **Visibility:** Enable/Disable Maintenance Mode.

- **Logo:** Set your Logo at the top of the Page. 

- **Title:** Set the Title you want to set at the top of Page

- **Heading:** Set Heading to show.

- **Text:** Set Text to display below heading.

- **End Date:** Set expiration date of Maintenance period. This will set remaining time in Counter.

- **Whitelist IP Addresses:** By default, all visitors will be able to access only Maintenance screen when Maintenance Mode is enabled. But, if you want to allow certain visitors to access site as normal, set their IP Adresses here in comma-separated format. Maintenance Mode will not be enabled for these IP Addresses. 

- **Facebook Link:** Set your Facebook Link to display in footer.

- **Twitter Link:** Set your Twitter Link to display in footer.

- **LinkedIn Link:** Set your LinkedIn Link to display in footer.

- **Github Link:** Set your Github Link to display in footer.

- **Instagram Link:** Set your Instagram Link to display in footer.

- **YouTube Link:** Set your YouTube Link to display in footer.

- **Footer Text:** Set the text you want to display in footer. You can format it as well.


Theme Layout Settings
=====================

You can set Layout related settings in this section.

- **Font Color:** Set the font color to display on entire page.

- **Theme Layout:** You can set either solid color background or image as background. Depending on the option selected, you can set color or image for layout.

- **Show Countdown:** Enable/disable Countdown timer.

- **Animate:** Enable/disable animation when countdown boxes are displayed for first time on page.

- **Countbox Style:** This option will be available only if Show Countdown is On. You can set style of counter box from following options: Square box, Circle with Progressbar, Countdown with Subscription box.

Following options will be displayed only if Countdown with Subscription box option is selected in Countbox Style

- **Subscription box Header:** Set Subscription box header.

- **Subscription Placeholder:** Set Subscription Placeholder.

- **Subscription Button Label:** Set Subscription Button Label.

Once you set all the settings, Click on Save button.