
.. include:: ../Includes.txt

============
Introduction
============

EXT:ns_lazy_load
================

.. figure:: Images/lazy-load.jpg
   :alt: Extension Banner 

What does it do?
================

Are you facing speed and performance issue due to assets on your TYPO3 site? If yes, This plug-an-play extension reduces the number of HTTP requests mechanism and improves the loading time.

Helpful Links
=============

.. Note::

	- Product: https://t3planet.com/lazy-load-typo3-extension-free
	- Front End Demo: https://demo.t3planet.com/t3-extensions/lazy-load
	- Typo3 Back End Demo: https://demo.t3planet.com/live-typo3/t3t-extensions/typo3/index.php?route=%2Fmain&token=fafdbd9c2e3650ec6e4c9680b445f63d21b23766
	- To make any domain-related changes or whitelist any development and staging domains, please reach our support center.- https://t3planet.com/support