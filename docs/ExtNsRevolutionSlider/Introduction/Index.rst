
.. include:: ../Includes.txt

============
Introduction
============

EXT:ns_revolution_slider
========================

.. figure:: Images/TYPO3-Revolution-Slider-Banner-2.jpg
   :alt: TYPO3-Revolution-Slider-Banner-2
   :width: 800px

.. _What-does-it-do:

What does it do?
================

Are you looking to utilize the full visual editing power with simple drag and drop functionality just like WordPress for your TYPO3 website without any manual configurations? You've got! Now, the world's most popular revolution slider is now available in TYPO3 CMS too.

Helpful Links
=============

.. Note::

	- Product: https://t3planet.com/revolution-slider-typo3-extension
	- TYPO3 Backend Live Demo: https://demo.t3terminal.com/live-typo3/t3t-extensions/typo3/?TYPO3_AUTOLOGIN_USER=editor-revolution-slider
	- Front End Demo: https://demo.t3planet.com/t3-extensions/revolution-slider
	- To make any domain-related changes or whitelist any development and staging domains, please reach our support center.- https://t3planet.com/support