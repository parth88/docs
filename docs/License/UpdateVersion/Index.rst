.. include:: ../Includes.txt

.. _installation:

==============
Update Version
==============

How Should I Know About New Version?
====================================

Whenever Team T3Planet will release new version for paricular for TYPO3 product, You will get an email information about new released version. Here is the sample email.

.. figure:: Images/NewVersionUpdate.jpeg
   :alt: New Version Update Email

Attention
=========
1. Before update your TYPO3 product, Please make sure to get latest version of EXT.ns_license https://extensions.typo3.org/extension/ns_license
2. We highly recommend to take a backup (code & database) of your whole TYPO3 instance. During the update, If any problem cause then you can roll back your TYPO3 instance. So, please take backup now before update :)

Minor Version Update
====================

What's minor version update? It means, if you want to update product with security update or minor bug fixing; Example 1.1.0 to 1.2.0 

**Step 1.** Go to NITSAN > License Management

**Step 2.** Check Your Product, Is "Latest Version" Marked? Then, you don't need to do anything.

.. figure:: Images/UptoDateVersion.jpeg
   :alt: Is New Update Availabe?

**Step 3.** If new updates available, then click on "Update to X.X.X" button of your particular product.

.. figure:: Images/GetLatestVersion.jpeg
   :alt: Get New Version


Major Version Updates
=====================

What's major version update? It means, if you want to update product with major branch version; Example 1.1.0 to 2.0.0

Step 1. Update TYPO3 Product
----------------------------

Perform above 3 steps to get latest version of TYPO3 product.

Step 2. Updated Dependent TYPO3 Extensions
----------------------------

**For Normal TYPO3 Instance**

- Go to Extensions Manager and Update all third party dependent TYPO3 extensions to particular T3Planet extension.
- Go to Admin Tools > Maintenance > Anaylze Database (if any)

**For Composer TYPO3 Instance**

- Run `composer update` Update all third party dependent TYPO3 extensions to particular  extension.
- Run `database:updateschema` (if any)


.. Tip:: Whenever you update T3Planet products, you need to do two things from the backend. First step is to "Dump Autoload" and the second step is to "Flush cache". You may do it via non-composer & composer mode according to your site setup.

**Composer Mode**

.. figure:: Images/Steps_After_Update_Composer.jpeg
   :alt: Steps After Update Composer

**Non-Composer Mode**

.. figure:: Images/Steps_After_Update_Non_Composer.jpeg
   :alt: Steps After Update Non Composer