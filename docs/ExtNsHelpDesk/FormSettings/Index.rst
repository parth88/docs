.. include:: ../Includes.txt

=============
Form Settings
=============

You can easily setup your ticket form from the typo3 backend. you can configure the popup support also.

**Step 1.** Go to Admin Tools > NS Helpdesk

**Step 2.** Click on "Form Setting" menu

.. figure:: Images/ns-helpdesk-typo3-form-settings1.jpeg
   :alt: ns-helpdesk-typo3-form-settings1
   
.. figure:: Images/ns-helpdesk-typo3-form-settings2.jpeg
   :alt: ns-helpdesk-typo3-form-settings2

.. figure:: Images/ns-helpdesk-typo3-form-settings4.jpeg
   :alt: ns-helpdesk-typo3-form-settings4

.. figure:: Images/ns-helpdesk-typo3-form-settings5.jpeg
   :alt: ns-helpdesk-typo3-form-settings5

.. Note:: Front end look of the ticket creation form.

.. figure:: Images/ns-helpdesk-typo3-form-fe.jpeg
   :alt: ns-helpdesk-typo3-form-fe

===================
Popup Form Settings
===================

**Step 1.** Go to Admin Tools > NS Helpdesk

**Step 2.** Click on "Form Setting" menu

.. figure:: Images/ns-helpdesk-typo3-popup-form-settings3.jpeg
   :alt: ns-helpdesk-typo3-form-settings


.. Note:: Front end look of the Popup support form

.. figure:: Images/ns-helpdesk-typo3-popup-form-fe.jpeg
   :alt: ns-helpdesk-typo3-popup-form-fe