.. include:: ../Includes.txt

.. _installation:

============
Installation
============

Just install this extension the usual way like any other TYPO3 extension.

For Premium Version - License Activation
========================================

To activate license and install this premium TYPO3 product, Please refer this documentation https://docs.t3planet.com/en/latest/License/Index.html


For Free Version
================

In the TYPO3 backend you can use the extension manager (EM).

**Step 1.** Switch to the module “Extension Manager”.

**Step 2.** Get the extension

**Step 3.** Get it from the Extension Manager: Press the “Retrieve/Update” button and search for the extension key ns_timeline and import the extension from the repository.

**Step 4.** Get it from typo3.org: You can always get the current version from https://extensions.typo3.org/extension/ns_timeline/ by downloading either the t3x or zip version. Upload the file afterwards in the Extension Manager.

.. figure:: Images/ns-timeline-typo3-install-extension.jpeg
   :alt: ns-timeline-typo3-install-extension

.. Note:: You must need to install the "News System" & "News Author" extension from the typo3 repository for style 8,9 & 10. Those style are dependent on these three News Style.


How to Install TYPO3 Extension ns_timeline
==========================================

**Extension Installation Via without Composer mode**
https://youtu.be/SN5HoFQcDM4

**Extension Via Composer**
https://youtu.be/_7ILu4lwU-k


