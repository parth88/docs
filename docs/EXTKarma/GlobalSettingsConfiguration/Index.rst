.. include:: ../Includes.txt

=============
Theme Options
=============

Global-Level Configuration
==========================

This module will help you configure global settings.

- Go to NITSAN > Theme Options
- Click on "Root/Main" page from Page-Tree
- You can configure General, SEO, GDPR, Style, Integration etc.
- All the theme options are self-explain, We recommend to configure everything once.

.. figure:: Images/T3Terminal-T3Karma-Theme-OptionsGeneral.jpeg
   :alt: T3Planet-T3Karma-Theme-OptionsGeneral

.. figure:: Images/T3Terminal-T3Karma-Theme-OptionsSEO.jpeg
   :alt: T3Planet-T3Karma-Theme-OptionsSEO

.. figure:: Images/T3Terminal-T3Karma-Theme-OptionsGDPR.jpeg
   :alt: T3Planet-T3Karma-Theme-OptionsGDPR
   
.. figure:: Images/T3Terminal-T3Karma-Theme-OptionsStyle.jpeg
   :alt: T3Planet-T3Karma-Theme-OptionsStyle

.. figure:: Images/T3Terminal-T3Karma-Theme-OptionsIntegration.jpeg
   :alt: T3Planet-T3Karma-Theme-OptionsIntegration
   

**General** : This tab consists of settings related to Header, Navigation menu, Footer & Site Maintenance.

**SEO** : This tab consist configurations related to SEO of the site.

**GDPR** : This tab manages the GDPR Cookiebar settings and its style.

**Style** : You can manage style of entire site from here. You can set global settings for elements like Header, Navigation Menu & footer from here. However, you can overwrite them at page level also. To overwrite these settings, go to Extended tab in Page properties.

**Integration** : You can add customised CSS and Integration scripts from third-party tools for Tracking purposes.


Page-Level Configuration
========================

From theme option tab, you can now directly select page, and create a extension template & configure desired style for particular page.

.. figure:: Images/t3karma-create-extension-template.jpeg
   :alt: T3Planet-TYPO3-Theme-T3Karma-create-extension-template
   
After creating extension template for the specific page, you can now configure all theme options for this particular page only.

.. figure:: Images/t3karma-page-level-theme-option.jpeg
   :alt: T3Planet-TYPO3-Theme-T3Karma-page-level-theme-option