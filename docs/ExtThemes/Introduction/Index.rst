
.. include:: ../Includes.txt

============
Introduction
============

About Our TYPO3 Templates
=========================
- Stable version
- Pre-configured
- Flexible Backend Management
- Fast, Lightweight & Powerful
- Optimized and Extendable
- Future Upgradable
- Followed TYPO3 Core Standards
- Highly compatible with TYPO3 extensions


TYPO3 Compatibility
===================

Most of our TYPO3 templates are compatible with all major TYPO3 LTS versions v8, v9 and v10.

Backend Technologies
====================

- TYPO3 Extbase
- Fluid Templates
- TypoScript

Extensions Dependencies
=======================

- EXT:fluid_styled_content
- EXT:ns_basetheme
- EXT:ns_child_themename

Extensions Dependencies (Optional)
==================================
- EXT:gridelements
- EXT:blog
- EXT:news

Frontend Techniques
===================

- HTML5
- CSS3
- Bootstrap (optional)
- SASS (optional)
- JavaScript
- jQuery OpenSource Plugins
- Other OpenSource Plugins