.. include:: ../Includes.txt

=============
Configuration
=============

.. toctree::

	AddMapLibrary/Index
	AddMapLocations/Index
	AddOpenStreetMapPlugin/Index