.. include:: ../Includes.txt

.. _installation:

============
Installation
============

Important Notes Before Installation
===================================

You should install the TYPO3 template at the new blank TYPO3 Instance! If you are trying to install the template in your existing used TYPO3 Instance, your current data may lose.


License Activation & TYPO3 Installation
=======================================

To activate license and install this premium TYPO3 product, Please refer this documentation https://docs.t3planet.com/en/latest/License/Index.html 


How to Install TYPO3 Template T3 Karma 
======================================

- **Normal Template Installation** https://youtu.be/OCf-cbsV-3U
- **Composer-based Template Installation ** https://youtu.be/swpOGTFMwf0


.. attention:: The Gridelement is not compatible with TYPO3 11 but its dev-master version is working, so run below command before installing t3-karma if your setup is with composer mode.

.. code-block:: python

   composer req gridelementsteam/gridelements:@dev

.. attention:: If your setup is without composer mode than just install the t3-karma theme & enable the checkbox "I Understand..." & click on the button "I know What I'm doing".

.. figure:: Images/installationnote.png
   :alt: Site Configuration
   
Site Management
===============

We have already provided Site Configuration and if you want to overwrite it then you can do it from Sites Module in Site Management.

.. figure:: Images/how_to_configure_site.jpeg
   :alt: Site Configuration


Page Tree
=========

Once you install your TYPO3 Template extension, it will automatically generate "Page tree" in your TYPO3 backend with all the pages and content.

.. figure:: Images/page_tree.jpeg
   :alt: Page Tree

.. note:: After installation, Most important is to set correct ID's of menu/pages to create proper menu and content. Sometime TYPO3 mis-configured Page-ID, Please go to General > Menu and Pages Settings and set appropriate page-ids.
