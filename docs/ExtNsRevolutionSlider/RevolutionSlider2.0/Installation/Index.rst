.. include:: ../Includes.txt

.. _installation:

============
Installation
============

License Activation & Installation
=================================

To activate license and install this premium TYPO3 product, Please refer this documentation https://docs.t3planet.com/en/latest/License/Index.html


Include the TypoScript
=======================

   The extension ships some static TypoScript code which needs to be included.

   .. rst-class:: bignums

   #. Switch to the root page of your site.

   #. Switch to the **Template module** and select *Info/Modify*.

   #. Click the link **Edit the whole template record** and switch to the tab
      *Includes*.

   #. Select **[NITSAN] Revolution Slider (ns_revolution_slider)** at the field *Include static
      (from extensions):*

   .. figure:: Images/TYPO3-Revolution-Slider-Include-Template.png
       :alt: TYPO3-Revolution-Slider-Include-Template
       :width: 1000px


#. Check System Requirements of Revolution Slider

   .. rst-class:: bignums

   #. Switch to NITSAN > Revolution Slider

   #. Check "System Requirements" section at Dashboard, Please make sure to have all green-signals ;)

   .. figure:: Images/TYPO3-Revolution-Slider-System-Requirement.png
       :alt: TYPO3-Revolution-Slider-System-Requirement
       :width: 1000px
	   
How to Install TYPO3 Extension ns_revoltionslider
=================================================

**Extension Installation Via without Composer mode**
https://youtu.be/OCf-cbsV-3U

**Extension Via Composer**
https://youtu.be/swpOGTFMwf0
  
	   