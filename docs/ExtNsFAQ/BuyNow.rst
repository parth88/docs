.. include:: ../Includes.txt

.. _faq:

==================
Get This Extension
==================

Get Free Version
================

Get Free version of this extension with basic-features from https://extensions.typo3.org/extension/ns_faq or https://t3planet.com/ns-faq-typo3-extension


Buy Pro Version
===============

You can buy Pro Version of this extension with more-features and free-support from https://t3planet.com/ns-faq-typo3-extension