.. include:: ../Includes.txt

.. _helpdesk:

==================
Get This Extension
==================

Get Free Version
================

Get Free version of this extension with basic-features from https://extensions.typo3.org/extension/ns_helpdesk or https://t3planet.com/typo3-help-desk-support-extension


Buy Pro Version
===============

You can buy Pro Version of this extension with more-features and free-support from https://t3planet.com/typo3-help-desk-support-extension