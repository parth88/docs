.. include:: ../Includes.txt

==============
For Developers
==============

ViewHelpers
===========

We have added a few ViewHelpers with this extension to get date of last comment in a News and to get count of number of comments in each News.

For this, you have to add a Namespace of ViewHelper.

.. code-block:: language

    xmlns:nscomment="http://typo3.org/ns/Nitsan/NsNewsComments/ViewHelpers"

Check this for more details:

.. figure:: Images/add_namespace.png
   :alt: Add namespace


Get Date of Last comment in a News
==================================

To get date of last comment in a news, add following code.

.. code-block:: language

    <f:for each="{nscomment:lastComment(newsuid: newsItem.uid)}" as="newsComment">
     | Last Comment on <f:format.date format="d.m.Y">{newsComment.crdate}</f:format.date>
    </f:for>

Check this for more details:

.. figure:: Images/get_date_of_last_comment.png
   :alt: Get date of Last Comment


Get count of number of comments in a News
=========================================

To get date of last comment in a news, add following code in List.html

.. code-block:: language

    Comments: {nscomment:count(newsuid: newsItem.uid)}

Check this for more details:

.. figure:: Images/get_count_of_comments.png
   :alt: Get count of comments

.. note:: Above ViewHelpers are supported in V8, V9 & V10 only.


Routing in TYPO3 9 & 10
=======================

Since version 9.5, TYPO3 supports speaking URLs and if you have used the typeNum for any Ajax related work then you also need to add **routeEnhancers** like **routeEnhancers > PageTypeSuffix > map > nsnewscomment: 99** in your site configuration.

.. code-block:: YAML

    routeEnhancers:
      PageTypeSuffix:
        type: PageType
        default: /
        index: ''
        map:
          /: 0
          nsnewscomment: 99


.. figure:: Images/route_enhancer.png
   :alt: Route Enhancer