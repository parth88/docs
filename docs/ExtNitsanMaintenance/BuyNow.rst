.. include:: ../Includes.txt

.. _faq:

==================
Get This Extension
==================

Get Free Version
================

Get Free version of this extension with basic-features from https://t3planet.com/typo3-maintenance-mode-extension-free or https://extensions.typo3.org/extension/nitsan_maintenance


Buy Pro Version
===============

You can buy Pro Version of this extension with more-features and free-support from https://t3planet.com/typo3-maintenance-mode-extension-pro