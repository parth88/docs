.. include:: ../Includes.txt

=======================
Comment Plugin Settings
=======================

Add Comment Plugin
==================

You can add News Comment plugin from Add element wizard

.. figure:: Images/add_plugin_wizard.jpeg
   :alt: Add News Comment Plugin

Now, set all the settings of Comment plugin

*Main Configuration*

.. figure:: Images/comment_plugin1.jpeg
   :alt: Comment Plugin - Main Configuration1

.. figure:: Images/comment_plugin2.jpeg
   :alt: Comment Plugin - Main Configuration2

**Custom Date Format:** Check this checkbox to use custom date format to display in Comment list.

**Date Format:** You can use any of the standard date format as well.

**Time Format:** You can use any of the standard time format as well.

**Captcha Settings:**  You can set whether to display Captcha or not. You can select one of the options from below:
   
- **None:** Disable Captcha

- **Image Captcha:** Display Image Captcha. It will look like this:

.. figure:: Images/image_captcha.jpeg
   :alt: Image Captcha Demo

If you are using Free version and have enabled captcha then Image Captcha will be displayed at comment form.

.. Note:: If you select Image Captcha, you need to rename _.htaccess file to .htaccess at this folder /typo3conf/ext/ns_news_comments/Resources/Private/ 

.. figure:: Images/captcha.jpeg
   :alt: Captcha


- **Google reCAPTCHA v2 :** Display Google reCAPTCHA v2. Make sure to add Sitekey in Constant. It will look like this:

.. figure:: Images/google-captcha.jpeg
   :alt: Google Captcha Demo


**Add User Image:** Add user image to display for all commments.
