.. include:: ../Includes.txt

==============
Customize Form
==============

Add your custom form fields from the customize form module. 

.. figure:: Images/ns-helpdesk-typo3-CustomizeFormAdd.jpeg
   :alt: ns-helpdesk-typo3-CustomizeFormAdd.jpeg


Create Custom Fields
====================

**Step 1.** Go to Admin Tools > NS Helpdesk

**Step 2.** Click on "Customize Form" menu

**Step 3.** Click on "Add New" Link

**Step 4.** Insert title to the group of form fields. Than after add a Title of specific field, Variable name & select the type of the field. Its a nested, so you may add your own form fields to suite your site.
We have given a Input field, Text area, Select field, Checkbox & radio button.

.. figure:: Images/ns-helpdesk-typo3-CustomizeForm.jpeg
   :alt: ns-helpdesk-typo3-CustomizeForm