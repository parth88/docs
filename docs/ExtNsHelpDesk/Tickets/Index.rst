.. include:: ../Includes.txt

==============
Ticket Listing
==============

You can easily create & see all tickets from tickets menu from your typo3 backend. you can find any tickets via filtration located at sidebar. as well as you can search any tickets within seconds via search feature.


**Step 1.** Go to Admin Tools > NS Helpdesk

**Step 2.** Click on Tickets Menu.

.. figure:: Images/ns-helpdesk-typo3-ticket-listing-be.jpeg
   :alt: ns-helpdesk-typo3-ticket-listing-be
   
  
.. Note:: Front end look of ticket listing page.

.. figure:: Images/ns-helpdesk-typo3-ticket-listing-fe.jpeg
   :alt: ns-helpdesk-typo3-ticket-listing-fe
  
   
=============
Ticket Detail
=============

**Step 1.** Click on specific ticket.

**Step 2.** Now admin Can add comment & close/reopen ticket from Backend as well.
 
.. figure:: Images/ns-helpdesk-typo3-ticket-detail1.jpeg
   :alt: ns-helpdesk-typo3-ticket-detail1

.. figure:: Images/ns-helpdesk-typo3-ticket-detail2.jpeg
   :alt: ns-helpdesk-typo3-ticket-detail2
         
.. Note:: Front end look of ticket Detail page.

.. figure:: Images/ns-helpdesk-typo3-ticket-detail-fe1.jpeg
   :alt: ns-helpdesk-typo3-ticket-detail-fe1
      
.. figure:: Images/ns-helpdesk-typo3-ticket-detail-fe2.jpeg
   :alt: ns-helpdesk-typo3-ticket-detail-fe2
