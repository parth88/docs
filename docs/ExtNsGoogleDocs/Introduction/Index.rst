
.. include:: ../Includes.txt

============
Introduction
============


EXT:ns_googledocs
=================

.. figure:: Images/ext_banner.jpg
   :alt: Extension Banner 

What does it do?
================

EXT:ns_googledocs is the only TYPO3 extension which allows Backend user to import Google Docs to TYPO3 and convert the Google Docs content into TYPO3 Content elements.  


**FEATURES:**




Screenshots
===========

Backend Screenshots
^^^^^^^^^^^^^^^^^^^

**Dashboard**

.. figure:: Images/dashboard_updated.jpeg
   :alt: Dashboard tab 

**Import Google Docs**

.. figure:: Images/import_google_docs.PNG
   :alt: Import Google Docs 

**Reports & Logs**

.. figure:: Images/reports_and_logs.png
   :alt: Reports & Logs tab 

**Global Settings**

.. figure:: Images/global_settings.jpeg
   :alt: Global Settings Tab 
  
  
Helpful Links
=============

.. Note::

	- Product: https://t3planet.com/ns-google-docs-typo3-extension
	- Front End Demo: https://demo.t3planet.com//t3t-extensions/googledocs
	- To make any domain-related changes or whitelist any development and staging domains, please reach our support center.- https://t3planet.com/support     