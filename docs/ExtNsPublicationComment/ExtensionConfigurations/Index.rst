.. include:: ../Includes.txt

========================
Extension Configurations
========================

First you need to install the publication extension from typo3 repository, After than install the ns_publication_comment that will enhance the detail view of each publication within comment module.
You can configure the comment settings like, enable/disable comment approval, change date/time format, add custom CSS/JS, E-mail configurations, Google recaptcha etc much more.

**Step 1.** Insert publication comment extension from the plugin element

**Step 2.** Select the record storage id, Edit date/time formate, captcha setting, user image etc according to your website need from main configurations.

**Step 3.** You can do the Comment user configurations also.

**Step 4.** Furthermore, another configurations will be managed from constant editor.

Back End Screenshots
====================

.. figure:: Images/ns-publication-comment-typo3-extension.jpeg
   :alt: ns-publication-comment-typo3-extension

.. figure:: Images/ns-publication-comment-typo3-extension1.jpeg
   :alt: ns-publication-comment-typo3-extension1

.. figure:: Images/ns-publication-comment-typo3-extension2.jpeg
   :alt: ns-publication-comment-typo3-extension2

.. figure:: Images/ns-publication-comment-typo3-extension3.jpeg
   :alt: ns-publication-comment-typo3-extension3
