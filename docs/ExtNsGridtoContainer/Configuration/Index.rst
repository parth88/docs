.. include:: ../Includes.txt

.. _configuration:

=============
Configuration
=============

Steps of Migration Process:
======================


Step 1: Before the Migration Process some steps are there you must be configured
==========================================

We assume you have EXT: grid elements and EXT: container Extension.
For migration, you must have to Container Configuration the same as Grid Configuration which is migrated.

   .. rst-class:: bignums

**For Example:**
   Grid Backend Layout Key 

.. figure:: Images/Grid_Backend_Layout_Key.jpg
            :alt: Grid Backend Layout Key
            :width: 1300px

**Container CType**

.. figure:: Images/containerCtype.jpg
            :alt: Container CType
            :width: 1300px

**For colPos:**
   Grid colPos

.. figure:: Images/Grid_colpos.jpg
            :alt: Container CType
            :width: 1300px

  While container colPos must be different like

 .. figure:: Images/Container_colpos.jpg
        :alt: container colPos
        :width: 1300px

  Because in the migration process, we need to make the same colPos of grid to the container. "$colPos = $element['tx_gridelements_columns'] + 100;"


Step 2: We provide 2 options for migrating grids
=============================================
Migration of all Grids available on the site.

.. figure:: Images/Migration.Jpg
            :alt: Migration
            :width: 1300px

  If you have a small number of girds then you can use the second option "Migration from grid elements layout key"

 .. figure:: Images/All_grid_layout.jpg
          :alt: All_grid_layout
          :width: 1300px


Process:
=================================

  .. rst-class:: bignums

 **Grid:**

 .. figure:: Images/grid.jpg
          :alt: Grid
          :width: 1300px

  .. rst-class:: bignums

 **Step 1:** Enter container CType with same as Grid Backend Layout Key

 .. figure:: Images/Grid_Backend_Layout_Key.jpg
          :alt: Grid_Layout_key
          :width: 1300px

  **Step 2:** Click On the "Migrate" Button then shows a success message 

 .. figure:: Images/Success_msg.jpg
          :alt: Success_Msg
          :width: 1300px

  **Step 3:** Now, check the migration

  .. figure:: Images/Migration_Done.Jpg
          :alt: Migration_Done
          :width: 1300px

  Also, this extension provides the hidden content elements for migration

 .. figure:: Images/hidden_1.jpg
          :alt: Hidden_1
          :width: 1300px

 .. figure:: Images/hidden_2.jpg
          :alt: Hidden_2
          :width: 1300px

 
 That's it, Now you can enjoy all the benifits of this extension :)
