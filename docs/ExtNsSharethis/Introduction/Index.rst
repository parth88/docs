
.. include:: ../Includes.txt

============
Introduction
============

EXT:ns_sharethis
================

   .. figure:: Images/TYPO3-EXTns_sharethis-Plugin-for-ShareThis-min.jpg
      :alt: TYPO3-EXTns_sharethis-Plugin-for-ShareThis-min
      :width: 1100px

.. _What-does-it-do:

What does it do?
================

One of the only TYPO3 extension is developed to integrate all the features of www.Sharethis.com which is one of the most popular third-party social media platform!

www.Sharethis.com is a free Social Media platform which provides professional and highly customizable ecosystem for social media sharing and optimizing of your valuable content. Take a look at their site at `https://www.sharethis.com/ <https://www.sharethis.com/>`_

It will increases traffic & engagement by helping people share your posts and pages to any service. Services include *Facebook*, *Twitter*, *Pinterest*, *Google*, *Gmail*, *LinkedIn* & "over 100 more" sharing and social media sites & apps.


Helpful Links
=============

.. Note::

	- Product: https://t3planet.com/sharethis-typo3-extension-free
	- Front End Demo: https://demo.t3planet.com//t3t-extensions/sharethis
	- To make any domain-related changes or whitelist any development and staging domains, please reach our support center.- https://t3planet.com/support