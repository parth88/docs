.. include:: Includes.txt

=======
Buy Now
=======

Get Free Version
================

Get Free version of this extension with basic-features from https://extensions.typo3.org/extension/ns_ext_compatibility/ or https://t3planet.com/typo3-extensions-compatibility-report-free/


Buy Pro Version
===============

You can buy Pro Version of this extension with more-features and free-support from https://t3planet.com/typo3-extensions-compatibility-report-pro/