	.. include:: ../Includes.txt

===========================
Isotope gallery at website?
===========================

Once you setup the isotope gallery view in Gallery module, you can use them in Gallery Plugins to display at your site.

.. figure:: Images/plugin_wizard_isotopeview.jpeg
   :alt: Gallery plugin wizard isotope
   
Once you add the plugin, switch to Plugin tab to configure the gallery. Here, You can set Gallery specific configuration.

.. figure:: Images/ns_gallery_isotope_view.jpeg
   :alt: Add Isotope View Plugin

Isotope view plugin has also 3 type of layout : Flex layout, Classic grid layout, & Pinterest layout. You can setup according to your website's need.!

.. Note:: All the configuration options will be same as Album view gallery & self-explanatory. :)