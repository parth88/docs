.. include:: ../Includes.txt

.. _installation:

============
Installation
============


Important notes before Installation
===================================

1) We recommend to install fresh TYPO3 installation, sometimes TYPO3 may faces difficulties to automatically create page tree and records.

2) For EXT:blog, you have to install the blog extension from Git repository here: https://github.com/TYPO3GmbH/blog


.. Note:: If you are installing Template using Composer then you will have to uninstall and re-install TYPO3 Template EXT:ns_theme_t3vishnu


License Activation & TYPO3 Installation
=======================================

To activate license and install this premium TYPO3 product, Please refer this documentation https://docs.t3planet.com/en/latest/License/Index.html


How to Install TYPO3 Template T3 Vishnu
=======================================

**Template Installation Via without Composer mode**
https://youtu.be/OCf-cbsV-3U
**Template Via Composer**
https://youtu.be/swpOGTFMwf0


Site Management
===============

We have already provided Site Configuration and if you want to overwrite it then you can do it from Sites Module in Site Management.

.. figure:: Images/T3Vishnu-TYPO3-Shop-Template-Site-Configuration.png
   :alt: T3Vishnu-TYPO3-Shop-Template-Site-Configuration


.. Note:: If you have composer based TYPO3 installation, then make create folder /config/ folder at your root, and copy/paste /public/typo3conf/ext/ns_basetheme/sites folder - to get default Site management configuration. 


Page Tree
=========

Once you install your TYPO3 Template extension, it will automatically generate "Page tree" in your TYPO3 backend with all the pages and content.

.. figure:: Images/T3Vishnu-TYPO3-Shop-Template-Page-Tree.png
   :alt: T3Vishnu-TYPO3-Shop-Template-Page-Tree


Shop's Dummy Products
=====================

For your convenience, Once you install aimeos & our theme extension, it will automatically import all dummy products to quickly initiate the shop template.

.. figure:: Images/T3Vishnu-TYPO3-Shop-Template-Products.png
   :alt: T3Vishnu-TYPO3-Shop-Template-Products