
.. include:: ../Includes.txt

============
Introduction
============


EXT:ns_gridtocontainer
================



What does it do?
================

What does it do?

EXT:ns_gridtocontainer is a migration extension providing a Backend module for those who want to switch from EXT: Gridelements to EXT:container.

FEATURES:

This Extension provides a backend module for the migration of grids to container
Also Mirgrating hidden content elements of Grid




Helpful Links
=============

.. Note::

	- Product: https://t3planet.com/gridtocontainer-typo3-extension-free
	- TYPO3 Backend Live Demo: https://demo.t3planet.com/live-typo3/t3t-extensions/typo3/?TYPO3_AUTOLOGIN_USER=editor-gridtocontainer
	- Front End Demo: https://demo.t3planet.com/t3-extensions/gridtocontainer	
	- To make any domain-related changes or whitelist any development and staging domains, please reach our support center.- https://t3planet.com/support