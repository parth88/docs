.. include:: ../Includes.txt

.. _whatsapp:

==================
Get This Extension
==================

Get Free Version
================

Get Free version of this extension with basic-features from https://extensions.typo3.org/extension/ns_whatsapp or https://t3planet.com/typo3-whatsapp-chat-extension


Buy Pro Version
===============

You can buy Pro Version of this extension with more-features and free-support from https://t3planet.com/typo3-whatsapp-chat-extension
