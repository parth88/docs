.. include:: ../Includes.txt

.. _faq:

=======
Buy Now
=======

Get Free Version
================

Get Free version of this extension with basic-features from https://extensions.typo3.org/extension/ns_news_slider/ or https://t3planet.com/news-slider-free/


Buy Pro Version
===============

You can buy Pro Version of this extension with more-features and free-support from https://t3planet.com/news-slider-pro/