.. include:: ../Includes.txt

=================
Ticket Categories
=================

To manage your ticket categories by setting up create,edit,delete,search ticket categories.

.. figure:: Images/ns-helpdesk-typo3-category-status.jpeg
   :alt: ns-helpdesk-typo3-category-status


Create Ticket Status
====================

**Step 1.** Go to Admin Tools > NS Helpdesk

**Step 2.** Click on "Categories" menu

**Step 3.** Click on "Add New" button

**Step 4.** Add category title, color and save Changes.

.. figure:: Images/ns-helpdesk-typo3-create-category-status.jpeg
   :alt: ns-helpdesk-typo3-create-category-status