.. include:: ../Includes.txt

=============
Ticket Status
=============

To manage your ticket status by setting up create,edit,delete,search ticket status.

.. figure:: Images/ns-helpdesk-typo3-ticket-status.jpeg
   :alt: ns-helpdesk-typo3-ticket-status


Create Ticket Status
====================

**Step 1.** Go to Admin Tools > NS Helpdesk

**Step 2.** Click on "Ticket Status" menu

**Step 3.** Click on "Add New" button

**Step 4.** Add status title, color & save Changes.

.. figure:: Images/ns-helpdesk-typo3-create-ticket-status.jpeg
   :alt: ns-helpdesk-typo3-create-ticket-status