
.. include:: ../Includes.txt

============
Introduction
============


EXT:ns_feedback
===============

.. figure:: Images/ext_ns_feedback_introduction.jpg
   :alt: Extension banner 

What does it do?
================

EXT:ns_feedback is a TYPO3 extension which allows website Administrators to add Feedback forms to the site and get valuable feedbacks or Insights from visitors. EXT:ns_feedback provides variety of feedback forms from which administrator can choose depending on the website page.

Following Feedback forms are available with this extension:

- **1. Ratings feedback Form**
- **2. Quick Feedback Form**
- **3. Full Feedback Form**
- **4. Pop-up Feedback Form**

Helpful Links
=============

.. Note::

	- Product: https://t3planet.com/ns-feedback-typo3-extension
	- TYPO3 Backend Live Demo: https://demo.t3planet.com/live-typo3/t3t-extensions/typo3/?TYPO3_AUTOLOGIN_USER=editor-feedback
	- Front End Demo: https://demo.t3planet.com/t3t-extensions/feedback
	- To make any domain-related changes or whitelist any development and staging domains, please reach our support center.- https://t3planet.com/support