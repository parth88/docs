
.. include:: ../Includes.txt

.. _configuration:

=============
Configuration
=============

Settings of your Webhook
========================

**Step 1:** Go to Admin Tools > Extension Configuration > EXT.ns_cache_webhook at TYPO3 Backend.

**Step 2:** Add "URL of your Webhook" and choose "Request Method" (Highly recommend to select POST)

.. figure:: Images/Configuration_TYPO3_Extension_Cache_Webhook.png
   :alt: Configuration_TYPO3_Extension_Cache_Webhook
   :width: 1000px

**Step 3:** Don't miss to click on "Save Configuration" button ;)