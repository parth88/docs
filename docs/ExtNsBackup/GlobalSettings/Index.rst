.. include:: ../Includes.txt

===============
Global Settings
===============

.. figure:: Images/ns-backup-typo3-global-validation.png
   :alt: ns-backup-typo3-global-validation

Once you install this extension, Your first-step should be to configure all the settings from "Global Configuration".

**Step 1.** Go to Admin Tools > NS Backup

**Step 2.** Click on "Global Settings" menu, Fill-up all the information and click on "Save Settings" button.

.. figure:: Images/ns-backup-typo3-global-settings.png
   :alt: ns-backup-typo3-global-settings