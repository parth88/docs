.. include:: ../Includes.txt

.. _installation:

==================
License Activation
==================


Install via Extension Manager
=============================

**Step 1.** Go to Extension module & Select “Get extensions” from the drop-down at top. Click on the Update Now button to get the Extension Repository.

.. figure:: Images/update-ext-repository.jpeg
   :alt: Update Extension Repository

**Step 2.** Now, Download the ns_license extension & install it.

.. figure:: Images/DownloadExtension.jpeg
   :alt: DownloadExtension

**Step 3.** Switch to NITSAN > License Management > Add Your License Key.

.. figure:: Images/LicenseModule.jpeg
   :alt: License Management

.. figure:: Images/LicenseActivated.jpeg
   :alt: License Activated

**Step 4.** Go to Admin Tools > Extensions > Activate Your Purchased Extension.

.. figure:: Images/InstallExtension.jpeg
   :alt: Install Extension

.. Attention::
   After actiation of extension, Go to Site Management > Site; And, if you do not found any site entry then re-activate (de-activate and activate again) the extension.


Install via Composer
====================

**Step 1.** Install EXT.ns_license extension

.. code-block:: python

   composer req nitsan/ns-license

**Step 2.** Go to Admin Tools > Extensions > Find "ns_license" > Deactivate & Activate.

.. attention:: For TYPO3 >= v11, Extension activate/de-activate feature not available from backend. Please consider to run below command.

.. code-block:: python

   php vendor/bin/typo3 extension:setup

**Step 3.** Switch to NITSAN > License Management > Add Your License Key.

.. figure:: Images/LicenseModule.jpeg
   :alt: License Management

.. figure:: Images/LicenseActivated.jpeg
   :alt: License Activated

**Step 4.** Run Composer Command

.. code-block:: python

   composer config repositories.nitsan path extensions/*
   composer req nitsan/[EXTENSION-KEY]:@dev

.. Attention::
   Get exact vendor and package name from your license activation email. eg., ```composer req nitsan/ns-theme-t3karma:@dev```

**Step 5.** Go to Admin Tools > Extensions > Deactivate & Activate again your purchased extension.

.. figure:: Images/InstallExtension.jpeg
   :alt: Install Extension

.. attention:: For TYPO3 >= v11, Extension activate/de-activate feature not available from backend. Please consider to run below command.

.. code-block:: python

   php vendor/bin/typo3 extension:setup

.. attention:: If you are installing `EXT.ns_revolution_slider`with TYPO3 >= v11 composer-based TYPO3 instance, then Please don't forget to run below commands.

.. code-block:: python

   php vendor/bin/typo3 extension:setup
   php vendor/bin/typo3 nsrevolution:setup
