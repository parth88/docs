.. include:: ../Includes.txt

.. _installation:

=======================
Migration from v1 to v2
=======================

We are feeling very excited to let you know, your dream feature-request is launched, now our TYPO3 revolution slider works as same as famous WordPress version. Get this extension, and create drag-n-drop & unlimited feature of backend revolution slider visual editor.

We have major breaking changes with v2, now whole approach to create slider is changed, you don't need to create storage folder, add slider image, choose options from frontend plugin etc.

To migrate from Revolution slider v1 to v2, You will need to perform below steps.

.. rst-class:: bignums-xxl

**Migrate from v1 to v2**

   Please follows the Step-by-step Guide to migrate from TYPO3 revolution slider v1 to v2.

   .. rst-class:: bignums

   #. Download latest revolution slider v2 from T3Planet > My Products > https://t3planet.com/downloadable/customer/products/

   #. Switch to the module “Extension Manager”.

   #. Find ns_revolution_slider extension, in-active & remove it.

   #. Go to NITSAN > License Management > De-Activate License, Check https://docs.t3planet.com/en/latest/License/LicenseDeActivation/Index.html#how-to-de-activate-license-key

   #. Follow Installation Steps at https://docs.t3planet.com/en/latest/ExtNsRevolutionSlider/RevolutionSlider2.0/Installation/Index.html

   #. Follow Configuration Steps at https://docs.t3planet.com/en/latest/ExtNsRevolutionSlider/RevolutionSlider2.0/Configuration/Index.html